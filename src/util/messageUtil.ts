// 消息处理工具类
import { message } from 'ant-design-vue'

import { ConfigType, MessageType, ConfigDuration, ConfigOnClose } from 'ant-design-vue/types/message'

export declare interface Message {
  success(content: ConfigType, duration?: ConfigDuration, onClose?: ConfigOnClose): MessageType
  error(content: ConfigType, duration?: ConfigDuration, onClose?: ConfigOnClose): MessageType
  info(content: ConfigType, duration?: ConfigDuration, onClose?: ConfigOnClose): MessageType
  warning(content: ConfigType, duration?: ConfigDuration, onClose?: ConfigOnClose): MessageType
  loading(content: ConfigType, duration?: ConfigDuration, onClose?: ConfigOnClose): MessageType
}
export const mpdMessage: Message = {
  success(content: ConfigType, duration?: ConfigDuration, onClose?: ConfigOnClose): MessageType {
    messageInfo.content = content
    messageInfo.type = 'success'
    return message.success(content, duration, onClose)
  },
  error(content: ConfigType, duration?: ConfigDuration, onClose?: ConfigOnClose): MessageType {
    messageInfo.content = content
    messageInfo.type = 'error'
    return message.error(content, duration, onClose)
  },
  info(content: ConfigType, duration?: ConfigDuration, onClose?: ConfigOnClose): MessageType {
    messageInfo.content = content
    messageInfo.type = 'info'
    return message.info(content, duration, onClose)
  },
  warning(content: ConfigType, duration?: ConfigDuration, onClose?: ConfigOnClose): MessageType {
    messageInfo.content = content
    messageInfo.type = 'warning'
    return message.warning(content, duration, onClose)
  },
  loading(content: ConfigType, duration?: ConfigDuration, onClose?: ConfigOnClose): MessageType {
    messageInfo.content = content
    messageInfo.type = 'loading'
    return message.loading(content, duration, onClose)
  }
}

export declare interface MessageInfox {
  content: ConfigType
  type: 'success' | 'info' | 'warning' | 'error' | 'loading'
}

// 提示信息字符串
export const messageInfo: MessageInfox = {
  content: '',
  type: 'info'
}
