/**
 * 判断字符串是否符合正则表达式
 * @param pattern
 * @param str
 */
export const regex = function(pattern: string | RegExp, str: string) {
  return new RegExp(pattern).test(str)
}
/**
 * 是否符合变量名规则
 * @param content
 */
export const isVariableName = function(content: string) {
  const pattern = /^[a-zA-Z_][a-zA-Z0-9_]*$/
  return pattern.test(content)
}
/**
 * 是否符合css变量名规则
 * @param content
 */
export const isCssVariableName = function(content: string) {
  const pattern = /^[a-zA-Z_][a-zA-Z0-9_-]*$/
  return pattern.test(content)
}
