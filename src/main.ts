import Vue from 'vue'
import App from './App.vue'
import MagicPageDesigner from './index'

Vue.config.productionTip = false

// 测试ant
// import { addAntLibrary } from './index'
// addAntLibrary()

Vue.use(MagicPageDesigner)
new Vue({
  render: h => h(App)
}).$mount('#app')
