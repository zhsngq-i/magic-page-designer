// 全局配置，主要用于设置

export default {
  // 展示辅助线
  showPageDomSubline: true,
  // 是否预览模式
  isPreviewMode: false,
  // 是否处于拖拽
  isDraggable: false,
  // 重新加载绘画区
  reloadCanvasValue: false,
  reloadCanvasFun() {
    this.reloadCanvasValue = true
  }
}
