import { PageDom, ConfigDetailType } from '@/library/page-object'
import { getBooleanSelect } from '@/library/ant'

const groupName: Array<string> = ['样式', '校验']

const data: Array<PageDom> = [
  {
    id: 'a-form-model',
    title: 'Model表单',
    ext: {
      help: 'FormModel 表单 (支持 v-model 检验)(版本：1.5.0+)',
      childrenComponent: ['a-form-model-item'],
      componentConfig: [
        {
          type: 'attr',
          title: '隐藏必选标记',
          name: 'hideRequiredMark',
          group: groupName[0],
          help: '隐藏所有表单项的必选标记',
          inputList: [getBooleanSelect('隐藏', '显示')]
        },
        {
          type: 'attr',
          title: 'label对齐方式',
          name: 'labelAlign',
          group: groupName[0],
          help: 'label标签的文本对齐方式',
          inputList: [
            {
              title: 'Select',
              component: ConfigDetailType.Select,
              attr: {
                allowClear: true,
                options: [
                  {
                    value: 'right',
                    label: '右侧'
                  },
                  {
                    value: 'left',
                    label: '左侧'
                  }
                ]
              }
            }
          ]
        },
        {
          type: 'attr',
          title: '表单布局',
          name: 'layout',
          group: groupName[0],
          inputList: [
            {
              title: 'Select',
              component: ConfigDetailType.Select,
              attr: {
                allowClear: true,
                options: [
                  {
                    value: 'horizontal',
                    label: '水平'
                  },
                  {
                    value: 'vertical',
                    label: '垂直'
                  },
                  {
                    value: 'inline',
                    label: '内联'
                  }
                ]
              }
            }
          ]
        },
        {
          type: 'attr',
          title: 'label标签布局',
          name: 'labelCol',
          group: groupName[0],
          help:
            'label 标签布局，同 <Col> 组件，设置 span offset 值，如 {span: 3, offset: 12} 或 sm: {span: 3, offset: 12}'
        },
        {
          type: 'attr',
          title: '布局样式',
          name: 'wrapperCol',
          group: groupName[0],
          help: '需要为输入控件设置布局样式时，使用该属性，用法同 labelCol'
        },
        {
          type: 'attr',
          name: 'colon',
          help: '配置 Form.Item 的 colon 的默认值 (只有在属性 layout 为 horizontal 时有效)'
        },
        {
          type: 'attr',
          name: 'validateOnRuleChange',
          help: '	是否在 rules 属性改变后立即触发一次验证',
          inputList: [getBooleanSelect('触发', '不触发')]
        }
      ]
    }
  },
  {
    id: 'a-form-model-item',
    title: 'Model表单item',
    mpdConfig: {
      slot: []
    },
    ext: {
      childrenComponent: [],
      parentComponent: ['a-form-model'],
      componentConfig: [
        {
          type: 'attr',
          title: '表单域model字段',
          name: 'prop',
          help: '在使用 validate、resetFields 方法的情况下，该属性是必填的'
        },
        {
          type: 'attr',
          title: '表单验证规则',
          name: 'rules',
          group: groupName[1]
        },
        {
          type: 'attr',
          title: '是否自动关联表单域',
          name: 'autoLink',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'attr',
          name: 'colon',
          group: groupName[0],
          help: '配合 label 属性使用，表示是否显示 label 后面的冒号',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'attr',
          title: '额外的提示信息',
          name: 'extra',
          group: groupName[1],
          help: '和 help 类似，当需要错误信息和提示文案同时出现时，可以使用这个'
        },
        {
          type: 'attr',
          name: 'hasFeedback',
          group: groupName[1],
          help: '配合 validateStatus 属性使用，展示校验状态图标，建议只配合 Input 组件使用',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'attr',
          title: '提示信息',
          name: 'help',
          group: groupName[0],
          help: '如不设置，则会根据校验规则自动生成'
        },
        {
          type: 'attr',
          name: 'htmlFor',
          help: '设置子元素 label htmlFor 属性'
        },
        {
          type: 'attr',
          title: '标签的文本',
          group: 'general',
          name: 'label'
        },
        {
          type: 'attr',
          title: 'label标签布局',
          name: 'labelCol',
          group: 'general',
          help:
            'label 标签布局，同 <Col> 组件，设置 span offset 值，如 {span: 3, offset: 12} 或 sm: {span: 3, offset: 12}'
        },
        {
          type: 'attr',
          title: 'label对齐方式',
          name: 'labelAlign',
          group: 'general',
          help: 'label标签的文本对齐方式',
          inputList: [
            {
              title: 'Select',
              component: ConfigDetailType.Select,
              attr: {
                allowClear: true,
                options: [
                  {
                    value: 'right',
                    label: '右侧'
                  },
                  {
                    value: 'left',
                    label: '左侧'
                  }
                ]
              }
            }
          ]
        },
        {
          type: 'attr',
          title: '是否必填',
          name: 'required',
          group: groupName[1],
          help: '如不设置，则会根据校验规则自动生成',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'attr',
          title: '校验状态',
          name: 'validateStatus',
          group: groupName[1],
          help: '如不设置，则会根据校验规则自动生成，可选：success、warning、error、validating'
        },
        {
          type: 'attr',
          name: 'wrapperCol',
          group: 'general',
          help: '需要为输入控件设置布局样式时，使用该属性，用法同 labelCol'
        }
      ]
    }
  }
]

export default data
