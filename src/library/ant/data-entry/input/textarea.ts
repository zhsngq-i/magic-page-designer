import { PageDom, ComponentItemExt, ComponentConfig } from '@/library/page-object'
import { getBooleanSelect } from '@/library/ant'
import input from './input'

const data: PageDom = {
  id: 'a-textarea',
  title: '文本域',
  ext: {
    componentConfig: [
      ...(<Array<ComponentConfig>>(<ComponentItemExt>input.ext).componentConfig),
      {
        type: 'attr',
        title: '自适应内容高度',
        name: 'autosize',
        group: 'general',
        help: '可设置为 true|false 或对象：{ minRows: 2, maxRows: 6 }',
        inputList: [getBooleanSelect()]
      }
    ]
  }
}

export default data
