import { PageDom } from '@/library/page-object'
import input from './input'
import textarea from './textarea'

const data: Array<PageDom> = [input, textarea]

export default data
