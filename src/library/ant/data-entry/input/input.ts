import { PageDom, ConfigDetailType } from '@/library/page-object'
import { getBooleanSelect, getSizeSelect } from '@/library/ant'

const groupName: Array<string> = ['样式']

const data: PageDom = {
  id: 'a-input',
  title: '输入框',
  mpdConfig: {
    slot: []
  },
  ext: {
    componentConfig: [
      {
        type: 'attr',
        title: '设置后置标签',
        name: 'addonAfter',
        group: groupName[0]
      },
      {
        type: 'attr',
        title: '设置前置标签',
        name: 'addonBefore',
        group: groupName[0]
      },
      {
        type: 'attr',
        title: '提示信息',
        name: 'placeholder',
        group: 'general'
      },
      {
        type: 'attr',
        title: '输入框默认内容',
        name: 'defaultValue',
        group: 'general'
      },
      {
        type: 'attr',
        title: '按钮失效状态',
        name: 'disabled',
        group: 'general',
        inputList: [getBooleanSelect('禁用', '启用')]
      },
      {
        type: 'attr',
        title: '最大长度',
        name: 'maxLength',
        group: 'general',
        inputList: [
          {
            title: '数字框',
            component: ConfigDetailType.InputNumber
          }
        ]
      },
      {
        type: 'attr',
        title: '带有前缀图标的input',
        name: 'prefix',
        group: groupName[0]
      },
      {
        type: 'attr',
        title: '控件大小',
        name: 'size',
        group: 'general',
        help: '标准表单内的输入框大小限制为large。可选：large、default、small',
        inputList: [getSizeSelect()]
      },
      {
        type: 'attr',
        title: '带有后缀图标的input',
        name: 'suffix',
        group: groupName[0]
      },
      {
        type: 'model',
        title: '输入框内容',
        name: 'value'
      },
      {
        type: 'attr',
        title: '清楚内容按钮',
        name: 'allowClear',
        group: 'general',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'event',
        title: '值改变事件',
        name: 'change'
      },
      {
        type: 'event',
        title: '按下回车事件',
        name: 'pressEnter'
      }
    ]
  }
}

export default data
