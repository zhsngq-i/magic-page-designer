import { PageDom, ConfigDetailType } from '@/library/page-object'
import { getBooleanSelect, getSizeSelect } from '@/library/ant'

const data: PageDom = {
  id: 'a-input-number',
  title: '数字输入框',
  ext: {
    componentConfig: [
      {
        type: 'attr',
        title: '自动获取焦点',
        name: 'autoFocus',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '初始值',
        name: 'defaultValue',
        group: 'general',
        inputList: [
          {
            title: '数字框',
            component: ConfigDetailType.InputNumber
          }
        ]
      },
      {
        type: 'attr',
        title: '是否禁用',
        name: 'disabled',
        group: 'general',
        inputList: [getBooleanSelect()]
      },
      {
        type: 'attr',
        title: '输入框展示格式',
        name: 'formatter',
        help: '指定输入框展示值的格式。function(value: number | string): string'
      },
      {
        type: 'attr',
        title: '最大值',
        name: 'max',
        group: 'general',
        inputList: [
          {
            title: '数字框',
            component: ConfigDetailType.InputNumber
          }
        ]
      },
      {
        type: 'attr',
        title: '最小值',
        name: 'min',
        group: 'general',
        inputList: [
          {
            title: '数字框',
            component: ConfigDetailType.InputNumber
          }
        ]
      },
      {
        type: 'attr',
        name: 'parser',
        help: '指定从 formatter 里转换回数字的方式，和 formatter 搭配使用。function( string): number'
      },
      {
        type: 'attr',
        title: '数值精度',
        name: 'precision',
        group: 'general',
        inputList: [
          {
            title: '数字框',
            component: ConfigDetailType.InputNumber
          }
        ]
      },
      {
        type: 'attr',
        title: '小数点',
        name: 'decimalSeparator'
      },
      {
        type: 'attr',
        title: '选择框大小',
        name: 'size',
        group: 'general',
        inputList: [getSizeSelect()]
      },
      {
        type: 'attr',
        title: '每次改变步数',
        name: 'step',
        group: 'general',
        help: '可以为小数',
        inputList: [
          {
            title: '数字框',
            component: ConfigDetailType.InputNumber
          }
        ]
      },
      {
        type: 'model',
        title: '当前值',
        name: 'value',
        inputList: [
          {
            title: '数字框',
            component: ConfigDetailType.InputNumber
          }
        ]
      },
      {
        type: 'event',
        title: '值变化回调',
        name: 'change',
        help: 'Function(value: number | string)'
      },
      {
        type: 'event',
        title: '按下回车的回调',
        name: 'pressEnter',
        help: 'function(e)'
      }
    ]
  }
}

export default data
