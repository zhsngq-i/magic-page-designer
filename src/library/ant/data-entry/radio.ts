import { PageDom, ConfigDetailType } from '@/library/page-object'
import { getBooleanSelect, getSizeSelect } from '@/library/ant'

const data: Array<PageDom> = [
  {
    id: 'a-radio-group',
    title: '单选框组',
    ext: {
      childrenComponent: ['a-radio'],
      componentConfig: [
        {
          type: 'attr',
          title: '默认选中的值',
          name: 'defaultValue',
          group: 'general'
        },
        {
          type: 'attr',
          title: '禁选所有子单选器',
          name: 'disabled',
          group: 'general',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'attr',
          title: 'name属性',
          name: 'name',
          group: 'general',
          help: 'RadioGroup 下所有 input[type="radio"] 的 name 属性'
        },
        {
          type: 'attr',
          title: '以配置形式设置子元素',
          name: 'options',
          group: 'general',
          help: 'string[] | Array<{ label: string value: string disabled?: boolean }>'
        },
        {
          type: 'attr',
          title: '选择框大小',
          name: 'size',
          group: 'general',
          inputList: [getSizeSelect()]
        },
        {
          type: 'model',
          title: '当前选中的值',
          name: 'value'
        },
        {
          type: 'attr',
          title: 'RadioButton的风格样式',
          name: 'buttonStyle',
          group: 'general',
          help: '目前有描边和填色两种风格',
          inputList: [
            {
              title: 'Select',
              component: ConfigDetailType.Select,
              attr: {
                allowClear: true,
                options: [
                  {
                    value: 'outline',
                    label: 'outline'
                  },
                  {
                    value: 'solid',
                    label: 'solid'
                  }
                ]
              }
            }
          ]
        },
        {
          type: 'event',
          title: '值变化时回调',
          name: 'change',
          help: 'Function(e:Event)'
        }
      ]
    }
  },
  {
    id: 'a-radio',
    title: '单选框',
    ext: {
      componentConfig: [
        {
          type: 'attr',
          title: '自动获取焦点',
          name: 'autoFocus',
          group: 'general',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'attr',
          title: '是否选中',
          name: 'checked',
          group: 'general',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'attr',
          title: '初始是否选中',
          name: 'defaultChecked',
          group: 'general',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'attr',
          title: '值',
          name: 'value',
          group: 'general'
        }
      ]
    }
  }
]

export default data
