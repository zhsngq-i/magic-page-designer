import { PageDom } from '@/library/page-object'
import { getBooleanSelect } from '@/library/ant'

const data: Array<PageDom> = [
  {
    id: 'a-checkbox-group',
    title: '多选框组',
    mpdConfig: {
      slot: []
    },
    ext: {
      childrenComponent: ['a-checkbox'],
      componentConfig: [
        {
          type: 'attr',
          title: '默认选中的值',
          name: 'defaultValue',
          group: 'general',
          help: 'string[]'
        },
        {
          type: 'attr',
          title: '是否禁用',
          name: 'disabled',
          group: 'general',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'attr',
          title: 'name属性',
          name: 'name',
          group: 'general',
          help: 'CheckboxGroup 下所有 input[type="checkbox"] 的 name 属性'
        },
        {
          type: 'attr',
          title: '以配置形式设置子元素',
          name: 'options',
          group: 'general',
          help:
            '指定可选项，可以通过 slot="label" slot-scope="option" 定制label。string[] | Array<{ label: string value: string disabled?: boolean, indeterminate?: boolean, onChange?: function }>'
        },
        {
          type: 'model',
          title: '当前选中的值',
          name: 'value',
          help: 'string[]'
        },
        {
          type: 'event',
          title: '值变化时回调',
          name: 'change',
          help: 'Function(checkedValue)'
        }
      ]
    }
  },
  {
    id: 'a-checkbox',
    title: '多选框',
    ext: {
      componentConfig: [
        {
          type: 'attr',
          title: '自动获取焦点',
          name: 'autoFocus',
          group: 'general',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'attr',
          title: '是否选中',
          name: 'checked',
          group: 'general',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'attr',
          title: '初始是否选中',
          name: 'defaultChecked',
          group: 'general',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'attr',
          title: '是否禁用',
          name: 'disabled',
          group: 'general',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'attr',
          title: '设置indeterminate状态',
          name: 'indeterminate',
          help: '只负责样式控制',
          group: 'general',
          inputList: [getBooleanSelect()]
        },
        {
          type: 'event',
          title: '值变化时回调',
          name: 'change',
          help: 'Function(e:Event)'
        }
      ]
    }
  }
]

export default data
