import { ComponentGroupInfo, ConfigDetailType } from '../page-object'
import { getBooleanSelect, getSizeSelect } from '@/library/ant'

const data: ComponentGroupInfo = {
  id: 'general',
  title: '通用',
  list: [
    {
      id: 'a-button-group',
      title: '按钮组',
      ext: {
        childrenComponent: ['a-button'],
        componentConfig: [
          {
            type: 'attr',
            name: 'size',
            group: 'general',
            inputList: [getSizeSelect()]
          }
        ]
      }
    },
    {
      id: 'a-button',
      title: '按钮',
      mpdConfig: {
        text: '按钮Text'
      },
      ext: {
        childrenComponent: [],
        componentConfig: [
          {
            type: 'attr',
            title: '按钮失效状态',
            name: 'disabled',
            group: 'general',
            inputList: [getBooleanSelect()]
          },
          {
            type: 'attr',
            title: '幽灵属性',
            name: 'ghost',
            inputList: [getBooleanSelect()]
          },
          {
            type: 'attr',
            title: '原生type值',
            name: 'htmlType',
            help:
              '设置 button 原生的 type 值，可选值请参考 HTML 标准。https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button#attr-type'
          },
          {
            type: 'attr',
            title: '按钮的图标类型',
            name: 'icon'
          },
          {
            type: 'attr',
            title: '按钮载入状态',
            name: 'loading',
            group: 'general',
            help: 'boolean | { delay: number }',
            inputList: [getBooleanSelect()]
          },
          {
            type: 'attr',
            title: '按钮形状',
            name: 'shape',
            group: 'general',
            inputList: [
              {
                title: 'Select',
                component: ConfigDetailType.Select,
                attr: {
                  allowClear: true,
                  options: [
                    {
                      value: 'circle',
                      label: 'circle'
                    },
                    {
                      value: 'round',
                      label: 'round'
                    }
                  ]
                }
              }
            ]
          },
          {
            type: 'attr',
            title: '按钮大小',
            name: 'size',
            group: 'general',
            inputList: [getSizeSelect()]
          },
          {
            type: 'attr',
            title: '按钮类型',
            name: 'type',
            group: 'general',
            inputList: [
              {
                title: 'Select',
                component: ConfigDetailType.Select,
                attr: {
                  allowClear: true,
                  options: [
                    {
                      value: 'default',
                      label: 'default'
                    },
                    {
                      value: 'primary',
                      label: 'primary'
                    },
                    {
                      value: 'dashed',
                      label: 'dashed'
                    },
                    {
                      value: 'danger',
                      label: 'danger'
                    },
                    {
                      value: 'link',
                      label: 'link'
                    }
                  ]
                }
              }
            ]
          },
          {
            type: 'attr',
            title: '宽度占满父元素',
            name: 'block',
            group: 'general',
            inputList: [getSizeSelect()]
          },
          {
            type: 'event',
            title: '点击按钮时的回调',
            name: 'click'
          }
        ]
      }
    }
  ]
}

export default data
