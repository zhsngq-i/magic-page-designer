import { ComponentGroupInfo, ConfigDetailType } from '../page-object'

const data: ComponentGroupInfo = {
  id: 'layout',
  title: '布局',
  list: [
    {
      id: 'a-row',
      title: 'Row',
      ext: {
        icon: 'mpd-icon-row',
        help: '通过`row`在水平方向建立一组`column`（简写 col）',
        childrenComponent: ['a-col'],
        componentConfig: [
          {
            type: 'attr',
            title: '栅格间隔',
            name: 'gutter',
            help:
              '栅格间隔，可以写成像素值或支持响应式的对象写法来设置水平间隔 { xs: 8, sm: 16, md: 24}。或者使用数组形式同时设置 [水平间距, 垂直间距]',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber,
                attr: {
                  max: 24,
                  min: 0
                }
              }
            ]
          },
          {
            type: 'attr',
            title: '布局模式',
            name: 'type',
            help: '布局模式，可选 flex，现代浏览器 下有效',
            inputList: [
              {
                component: ConfigDetailType.Select,
                attr: {
                  allowClear: true,
                  options: [
                    {
                      value: 'flex',
                      label: 'flex'
                    }
                  ]
                }
              }
            ]
          },
          {
            type: 'attr',
            title: '垂直对齐方式',
            name: 'align',
            help: 'flex 布局下的垂直对齐方式',
            inputList: [
              {
                component: ConfigDetailType.Select,
                attr: {
                  allowClear: true,
                  options: [
                    {
                      value: 'top',
                      label: '顶部'
                    },
                    {
                      value: 'middle',
                      label: '中间'
                    },
                    {
                      value: 'bottom',
                      label: '底部'
                    }
                  ]
                }
              }
            ]
          },
          {
            type: 'attr',
            title: '水平排列方式',
            name: 'justify',
            help: '	flex 布局下的水平排列方式',
            inputList: [
              {
                component: ConfigDetailType.Select,
                attr: {
                  allowClear: true,
                  options: [
                    {
                      value: 'start',
                      label: 'start'
                    },
                    {
                      value: 'end',
                      label: 'end'
                    },
                    {
                      value: 'center',
                      label: 'center'
                    },
                    {
                      value: 'space-around',
                      label: 'space-around'
                    },
                    {
                      value: 'space-between',
                      label: 'space-between'
                    }
                  ]
                }
              }
            ]
          }
        ]
      }
    },
    {
      id: 'a-col',
      title: 'Col',
      ext: {
        icon: 'mpd-icon-col',
        help: '你的内容应当放置于`col`内，并且，只有`col`可以作为`row`的直接元素',
        childrenComponent: [],
        parentComponent: ['a-row'],
        componentConfig: [
          {
            type: 'attr',
            title: 'flex 布局填充',
            name: 'flex',
            group: 'Flex',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber
              }
            ]
          },
          {
            type: 'attr',
            title: '偏移量',
            name: 'offset',
            group: 'general',
            help: '栅格左侧的间隔格数，间隔内不可以有栅格',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber
              }
            ]
          },
          {
            type: 'attr',
            title: '栅格顺序',
            name: 'order',
            group: 'Flex',
            help: 'flex 布局模式下有效',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber
              }
            ]
          },
          {
            type: 'attr',
            title: '栅格向左移动格数',
            name: 'pull',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber
              }
            ]
          },
          {
            type: 'attr',
            title: '栅格向右移动格数',
            name: 'push',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber
              }
            ]
          },
          {
            type: 'attr',
            name: 'span',
            group: 'general',
            help: '为 0 时相当于 display: none',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber,
                attr: {
                  max: 24,
                  min: 0
                }
              }
            ]
          },
          {
            type: 'attr',
            name: 'xs',
            group: '栅格占位格数',
            help: '<576px 响应式栅格，可为栅格数或一个包含其他属性的对象',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber,
                attr: {
                  max: 24,
                  min: 0
                }
              }
            ]
          },
          {
            type: 'attr',
            name: 'sm',
            group: '栅格占位格数',
            help: '≥576px 响应式栅格，可为栅格数或一个包含其他属性的对象',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber,
                attr: {
                  max: 24,
                  min: 0
                }
              }
            ]
          },
          {
            type: 'attr',
            name: 'md',
            group: '栅格占位格数',
            help: '≥768px 响应式栅格，可为栅格数或一个包含其他属性的对象',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber,
                attr: {
                  max: 24,
                  min: 0
                }
              }
            ]
          },
          {
            type: 'attr',
            name: 'lg',
            group: '栅格占位格数',
            help: '≥992px 响应式栅格，可为栅格数或一个包含其他属性的对象',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber,
                attr: {
                  max: 24,
                  min: 0
                }
              }
            ]
          },
          {
            type: 'attr',
            name: 'xl',
            group: '栅格占位格数',
            help: '≥1200px 响应式栅格，可为栅格数或一个包含其他属性的对象',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber,
                attr: {
                  max: 24,
                  min: 0
                }
              }
            ]
          },
          {
            type: 'attr',
            name: 'xxl',
            group: '栅格占位格数',
            help: '≥1600px 响应式栅格，可为栅格数或一个包含其他属性的对象',
            inputList: [
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber,
                attr: {
                  max: 24,
                  min: 0
                }
              }
            ]
          }
        ]
      }
    },
    {
      id: 'a-space',
      title: '间距',
      mpdConfig: {
        text: '请添加元素后删除该text'
      },
      ext: {
        help: '设置组件之间的间距，因为如果没有内容，该组件则不渲染，所以添加一个默认text',
        childrenComponent: [],
        componentConfig: [
          {
            type: 'attr',
            title: '对齐方式',
            name: 'align',
            inputList: [
              {
                title: '选择框',
                component: ConfigDetailType.Select,
                attr: {
                  allowClear: true,
                  options: [
                    {
                      value: 'start',
                      label: 'start'
                    },
                    {
                      value: 'end',
                      label: 'end'
                    },
                    {
                      value: 'center',
                      label: 'center'
                    },
                    {
                      value: 'baseline',
                      label: 'baseline'
                    }
                  ]
                }
              }
            ]
          },
          {
            type: 'attr',
            title: '间距方向',
            name: 'direction',
            inputList: [
              {
                title: '选择框',
                component: ConfigDetailType.Select,
                attr: {
                  allowClear: true,
                  options: [
                    {
                      value: 'vertical',
                      label: 'vertical'
                    },
                    {
                      value: 'horizontal',
                      label: 'horizontal'
                    }
                  ]
                }
              }
            ]
          },
          {
            type: 'attr',
            title: '间距大小',
            name: 'size',
            inputList: [
              {
                title: '选择框',
                component: ConfigDetailType.Select,
                attr: {
                  allowClear: true,
                  options: [
                    {
                      value: 'small',
                      label: 'small'
                    },
                    {
                      value: 'middle',
                      label: 'middle'
                    },
                    {
                      value: 'large',
                      label: 'large'
                    }
                  ]
                }
              },
              {
                title: '数字框',
                component: ConfigDetailType.InputNumber,
                attr: {
                  min: 0
                }
              }
            ]
          }
        ]
      }
    }
  ]
}

export default data
