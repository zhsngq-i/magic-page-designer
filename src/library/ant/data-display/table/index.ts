import { PageDom } from '@/library/page-object'
import table from './table'

const data: Array<PageDom> = [table]

export default data
