import { ComponentGroupInfo } from './index'

const data: ComponentGroupInfo = {
  id: 'layout',
  title: '布局',
  list: [
    {
      id: 'div',
      title: 'div',
      ext: {
        icon: 'mpd-icon-div',
        childrenComponent: []
      }
    },
    {
      id: 'mpd-html',
      title: 'Html',
      ext: {
        icon: 'mpd-icon-html',
        componentConfig: [
          {
            type: 'attr',
            name: 'htmlText',
            title: 'Html内容',
            group: 'general',
            help:
              '改字符串内容会被放入eval中执行，所以可以直接绑定字段使用，例如：这是展示的arg1参数的值${this.mpdPageValue.arg1}'
          }
        ]
      }
    }
  ]
}

export default data
