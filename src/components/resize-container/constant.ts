/**
 * 滚动条支持的方位
 */
export const positionArrays = ['top', 'right', 'bottom', 'left']

/**
 * 滚动条相关配置
 */
export const barConfigs = {
  // 水平的属性配置
  horizontal: {
    axis: 'X',
    offset: 'scrollLeft',
    sizeAttr: 'width',
    clientAxis: 'clientX',
    translate: 'translateX',
    direction1: 'left',
    direction2: 'right',
    wheelDelta: 'deltaX'
  },
  // 垂直的属性配置
  vertical: {
    axis: 'Y',
    offset: 'scrollTop',
    sizeAttr: 'height', // 滚动条的高度
    clientAxis: 'clientY', // 拖动滚动条时,鼠标移动的Y轴坐标值
    translate: 'translateY', // 上下移动滚动条的位置
    direction1: 'top', // 滚动条容器的top值,会与clientY发生计算
    direction2: 'bottom',
    wheelDelta: 'deltaY' // 在滚动条容器中滚动鼠标滚轮时,滚动量的值
  }
}

/**
 * 判断postition的方位
 * @param {String} position 位置
 */
export function verticalOrHorizontal(position: string) {
  switch (position) {
    case 'top':
    case 'bottom':
      // 纵向
      return 'vertical'
    case 'left':
    case 'right':
      // 横向
      return 'horizontal'
  }
  return ''
}
