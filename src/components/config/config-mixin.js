export default {
  props: {
    // 传入的page配置的值
    value: {},
    // 传入的配置项的值
    mpdItem: {
      type: Object,
      required: true
    }
  },
  data() {
    return {
      tmpData: null
    }
  },
  watch: {
    tmpData: {
      handler(val) {
        this.$emit('change', val)
      },
      deep: true
    }
  },
  created() {
    this.tmpData = this.value || this.mpdItem.defaultValue
  }
}
