// 自定义组件
import ScrollContainer from './scroll-container'
import ResizeContainer from './resize-container'
import MpdIcon from './mpd-icon'
import MpdText from './mpd-text'
import MpdTooltip from './mpd-tooltip'
import MpdHtml from './mpd-html'
import Vue from 'vue'

export default {
  install(app: typeof Vue) {
    // 自定义组件
    app.component(ScrollContainer.name, ScrollContainer)
    app.component(ResizeContainer.name, ResizeContainer)
    app.component(MpdIcon.name, MpdIcon)
    app.component(MpdText.name, MpdText)
    app.component(MpdTooltip.name, MpdTooltip)
    app.component(MpdHtml.name, MpdHtml)
  }
}
