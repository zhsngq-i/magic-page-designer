const path = require('path')
const resolve = (dir) => {
  return path.join(__dirname, dir)
}
// 设置环境变量，可以在全局使用
process.env.VUE_APP_MA_VERSION = require('./package.json').version

module.exports = {
  pages: {
    index: {
      // page 的入口
      entry: 'src/main.ts'
    }
  },
  publicPath: '/',
  configureWebpack: {
    output: {
      libraryExport: 'default'
    }
  },
  chainWebpack: (config) => {
    config.resolve.alias.set('@', resolve('src')) // key,value自行定义，比如.set('@@', resolve('src/components'))
    // 移除 prefetch 插件
    config.plugins.delete('prefetch')
    // 移除 preload 插件
    config.plugins.delete('preload')
    config.output.globalObject('this')
    config.output.filename('js/[name].[hash].js').end()
  },
  devServer: {
    host: '127.0.0.1',
    port: 8888
  }
}
