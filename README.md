# magic-page-designer

#### 介绍

`magic-page-designer`是一个基于 vue 的在线页面快速开发平台，主要把所有的`vue`代码都改为在线配置，最终获得一个`json`格式的页面，你可以把它存放到服务器的任何地方：数据库、静态文件、redis 等。最方便的是可以在线修改页面，再也不用担心生产出问题了，还得打开`IDE`，修改测试打包，直接在线编码，所见即所得。还可以配合 [magic-api](https://gitee.com/ssssssss-team/magic-api) 使用，完全抛弃`IDE`，随时随地`code with me`。

本框架的主要功能要区别于百度的`amis`，`amis`主要是可以在`web`上编辑一些页面上的元素，但是这些元素只能是`amis`框架中自己的已有的内容，所以当开发者想自己加一个甲方奇奇怪怪的需求的时候就可能会遇到难点。所以本框架有着`万物皆可创造`的思想：

- 譬如你的系统用的是`ant`框架，没问题，只需要在开发阶段，把`ant`的组件一个一个配置到框架中，那么`mpd`就拥有了在页面上拖拽配置`ant`的功能。
- 又或者你的系统用的是`element`框架，还是没问题，依旧把`element`配置到框架中，那么`mpd`就拥有了在页面上拖拽配置`element`的功能。
- 假如你的系统是一个闭源框架，完全是由内部自用，也没问题，把你自己写的所有组件配置到框架中，那么`mpd`就拥有了在页面上拖拽配置内部框架的功能。
- 所以`mpd`所提供的功能就是让你自己的`vue`组件，自由的在`web`上拖拽，并且实现对应的功能
- 而本框架的自由度极高，例如你的组件有一个属性值`size`，它的值可以取固定值`small、middle`，你可以直接配置：`input`直接输入、`select`直接选择、任何的组件。只要这个组件能给这个属性赋值，那就都可以用，而且支持`js`的原生代码
- 本框架还有更多的深层次的思想需要理解，所以不要仅仅只限于在`web`上拖拽组件这么简单，期待大家来发现

#### 特性

- 使用`vue2`，并且轻度依赖`ant-design-vue`(按需加载)，主要用于一些自用的配置组件，并不是强依赖，所以不用在意自己的框架是否引入`ant`
- 无需重复开发`vue`文件，只需要将注册到项目的`component`配置到左侧工具栏，就可以在线拖拽生成页面了
- 提供在线拖拽编辑页面，可即时修改页面不足之处，再也不用 `dev、build` 啦~
- 只需要添加到自己的项目中，再写一个自定义组件的`json`配置，就可以在页面上配置
- 没有任何限制，完全基于`component`自定义组件原理，支持属性、样式、事件配置，只要 vue 文件能写得出，那就能在平台上使用，而且写法完全一样
- 属性、双向绑定、事件等表达式写法，完全基于`js`和`vue`写法实现，无需额外的学习成本

#### 演示

点击前往[magic-page-designer-example](https://ssssssss-team.gitee.io/magic-page-designer-example/#/dashboard)

#### 安装教程

- `npm`命令(vue2)：`npm i magic-page-designer`
- `npm`命令(vue3)：`npm i magic-page-designer@next`
- `yarn`命令(vue2)：`yarn add magic-page-designer`
- `yarn`命令(vue3)：`yarn add magic-page-designer@next`

#### 使用说明

1.  注册组件

```js
import Vue from 'vue'
import App from './App.vue'
import MagicPageDesigner from 'magic-page-designer'
// 最后引入自定义css，为了覆盖其它样式
import 'magic-page-designer/dist/magic-page-designer.css'

Vue.config.productionTip = false

Vue.use(MagicPageDesigner)
new Vue({
  render: h => h(App)
}).$mount('#app')
```

2.  使用组件

```vue
<template>
  <magic-page-designer
    :mpdPageConfig.sync="mpdPageConfig"
    :mpdPageValue.sync="mpdPageValue"
    :selectPageNodeDom.sync="selectPageNodeDom"
    :editorMode="editorMode"
    @mpdSave="mpdSave"
  ></magic-page-designer>
</template>

<script>
export default {
  data() {
    return {
      // 需要渲染的页面json数据，必填
      mpdPageConfig: {},
      // 当前页面所存储的值
      mpdPageValue: {},
      // 编辑模式下选中的dom，非必填
      selectPageNodeDom: {},
      // 是否编辑模式，默认false
      editorMode: true
    }
  },
  methods: {
    // 保存事件
    mpdSave(mpdPageConfigVal) {
      console.log('mpdSave', mpdPageConfigVal)
    }
  }
}
</script>
```

3.  注册组件库，以便拖拽组件使用

```js
import { addLibrary } from 'magic-page-designer'
addLibrary({
  id: 'text',
  title: '文本',
  list: [
    {
      id: 'span',
      title: 'span',
      mpdConfig: {
        text: '在此处修改span标签内容'
      },
      ext: {
        icon: 'mpd-icon-text-border'
      }
    }
  ]
})
```

4. 更多详细说明请查看文档[magic-page-designer](https://ssssssss-team.gitee.io/magic-page-designer/)
